import asyncio
import logging

import bezoar.runtime.engine
from bezoar.console.console import Console


async def main_loop() -> None:
    console = Console()

    def emit(s: str) -> None:
        console.print(s)
    await bezoar.runtime.engine.start(emit)
    while True:
        command = await console.read_line("> ")
        if not command:
            continue
        if command in ["quit", "exit"]:
            emit("Bye!")
            break
        await bezoar.runtime.engine.submit_command(command)


def run() -> int:
    asyncio.run(main_loop())
    return 0


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    exit(run())
