import asyncio
import os
import sys
import termios
import tty
from asyncio.queues import Queue
from socket import SOCK_DGRAM, SOL_UDP, socket, socketpair
from typing import Optional

from bezoar.console import key_stroke
from bezoar.console.key_stroke import KeyStroke

_TranslationMap = dict[str, KeyStroke]


def _create_translation_table() -> _TranslationMap:
    table = _TranslationMap()
    for code in KeyStroke.codes():
        key = KeyStroke.by_code(code)
        if key:
            table[chr(code)] = key
    table[b"\x1b[A".decode()] = key_stroke.UP
    table[b"\x1b[B".decode()] = key_stroke.DOWN
    table[b"\x1b[C".decode()] = key_stroke.LEFT
    table[b"\x1b[D".decode()] = key_stroke.RIGHT
    table[b"\x1b[2~".decode()] = key_stroke.INSERT
    table[b"\x1b[H".decode()] = key_stroke.HOME
    table[b"\x1b[5~".decode()] = key_stroke.PAGE_UP
    table[b"\x1b[3~".decode()] = key_stroke.DELETE
    table[b"\x1b[F".decode()] = key_stroke.END
    table[b"\x1b[6~".decode()] = key_stroke.PAGE_DOWN
    # Override DEL to BS to make consistent with Windows
    table[b"\x7f".decode()] = key_stroke.BACKSPACE
    return table


_TRANSLATION_TABLE = _create_translation_table()

(_wakeup_read_socket, _wakeup_write_socket) = socketpair()
_wakeup_read_socket.setblocking(False)

class _StdInReader(object):
    def _reader(r: '_StdInReader') -> None:
        r._got_data()

    def __init__(self) -> None:
        super().__init__()
        self._queue = Queue[bytes]()
        self._stdin_fd = sys.stdin.fileno()
        self._running = False

    def start(self) -> None:
        if self._running:
            return
        self._running = True
        self._old_settings = termios.tcgetattr(self._stdin_fd)
        self._old_blocking = os.get_blocking(self._stdin_fd)  # type: ignore

        new_settings = termios.tcgetattr(self._stdin_fd)
        new_settings[3] = new_settings[3] & ~(termios.ICANON | termios.ECHO)
        termios.tcsetattr(self._stdin_fd, termios.TCSADRAIN,
                          new_settings)
        os.set_blocking(self._stdin_fd, False)  # type: ignore
        loop = asyncio.get_running_loop()
        loop.add_reader(self._stdin_fd, _StdInReader._reader, self)

    def stop(self) -> None:
        if not self._running:
            return
        self._running = False
        loop = asyncio.get_running_loop()
        loop.remove_reader(self._stdin_fd)
        termios.tcsetattr(self._stdin_fd, termios.TCSADRAIN,
                          self._old_settings)
        os.set_blocking(self._stdin_fd, self._old_blocking)  # type: ignore

    def _got_data(self) -> None:
        data = os.read(self._stdin_fd, 1024)
        self._queue.put_nowait(data)

    async def read(self) -> bytes:
        return await self._queue.get()


_wakeup_reader: Optional[asyncio.StreamReader] = None
_stdin_reader: Optional[_StdInReader] = None

_DEFAULT_LIMIT = 2 ** 16


async def _initialize_reader() -> None:
    global _wakeup_reader
    global _stdin_reader
    if _wakeup_reader is not None:
        return

    loop = asyncio.get_running_loop()
    waiter = loop.create_future()
    reader = asyncio.StreamReader(limit=_DEFAULT_LIMIT, loop=loop)
    protocol = asyncio.StreamReaderProtocol(reader, loop=loop)
    transport: asyncio.Transport = loop._make_socket_transport(  # type: ignore
        _wakeup_read_socket, protocol, waiter)
    try:
        await waiter
    except:
        transport.close()
    _wakeup_reader = reader
    _stdin_reader = _StdInReader()


def wake_get_key() -> None:
    """
    Wakes any outstanding call to get_key, causing it to return None
    immediately.
    """

    while _wakeup_write_socket.send(b"\x01") < 1:
        pass


async def get_key() -> Optional[KeyStroke]:
    """
    Wait for a key to be hit, returning the associated KeyStroke. Returns None
    if woken up before a key was pressed.
    """

    await _initialize_reader()
    if not _stdin_reader:
        return None
    if not _wakeup_reader:
        return None
    _stdin_reader.start()
    try:
        while True:
            wakeup_task = asyncio.create_task(_wakeup_reader.read(1042))
            stdin_task = asyncio.create_task(_stdin_reader.read())
            waiter = asyncio.wait({wakeup_task, stdin_task},
                                  return_when=asyncio.FIRST_COMPLETED)
            (done, pending) = await waiter
            for x in pending:
                x.cancel()

            if wakeup_task in done:
                return None

            if stdin_task not in done:
                continue

            data = stdin_task.result()
            characters = data.decode()
            key = _TRANSLATION_TABLE.get(characters)
            if key is None:
                continue
            if key == key_stroke.CANCEL:
                raise KeyboardInterrupt
            return key
    finally:
        _stdin_reader.stop()
