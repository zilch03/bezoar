from __future__ import annotations

from bezoar.base.text import normalize_paragraphs
from typing import Any, Callable, Iterable, Optional, Union


class EntityData(object):
    """
    Base class for all game objects.
    """

    Properties = dict[str, Any]
    DynamicString = Union[str, Callable[['EntityData'], str]]
    DynamicEntity = Union['EntityData',
                          str,
                          Callable[['EntityData'], 'EntityData']]
    Contents = list[DynamicEntity]

    ENTITY_DATA: dict[str, EntityData] = {}

    def evaluate_dynamic_string(entity: EntityData,
                                value: DynamicString) -> Optional[str]:
        if value is None:
            return None
        if callable(value):
            return value(entity)
        return value

    def evaluate_dynamic_entity(entity: EntityData,
                                value: DynamicEntity) -> Optional[EntityData]:
        if value is None:
            return None
        if isinstance(value, str):
            return find_data(value)
        if callable(value):
            return value(entity)
        if isinstance(value, EntityData):
            return value
        raise ValueError(f"Not a DynamicEntity: {value.__class__.__name__}")

    def __init__(
            self,
            id: str,
            name: DynamicString,
            description: DynamicString,
            contents: Contents,
            properties: Properties
    ) -> None:
        self._id = id
        self._name = name
        self._description = description
        self._contents = contents
        self._properties = properties
        if self._id in EntityData.ENTITY_DATA:
            raise ValueError(f"Existing ID: {self._id}")
        EntityData.ENTITY_DATA[self._id] = self

    def __str__(self) -> str:
        return f"<{self.__class__.__name__} id={self._id}>"

    def resolve_links(self) -> None:
        pass

    def instantiate(self) -> Entity:
        return Entity(self)

    @property
    def id(self) -> str:
        return self._id

    @property
    def name(self) -> str:
        result = EntityData.evaluate_dynamic_string(self, self._name)
        if not result:
            return ""
        return result

    @property
    def description(self) -> str:
        result = EntityData.evaluate_dynamic_string(self, self._description)
        if not result:
            return ""
        return result

    @property
    def properties(self) -> Properties:
        return self._properties

    @property
    def contents(self) -> Contents:
        return self._contents


class Entity(object):
    ENTITIES: dict[str, list[Entity]] = {}
    ENTITIES_BY_COUNTER: dict[int, Entity] = {}
    _COUNTER = 0

    def __init__(self, data: EntityData) -> None:
        super().__init__()
        self._data = data
        self._counter = Entity._COUNTER
        Entity._COUNTER += 1
        self._environment: Optional[Entity] = None
        self._contents: list[Entity] = []
        id = self.id
        if id not in Entity.ENTITIES:
            Entity.ENTITIES[id] = list[Entity]()
        id_list = Entity.ENTITIES[id]
        if self in id_list:
            raise RuntimeError(f"Entity ID already registered: {id}")
        id_list.append(self)
        if self._counter in Entity.ENTITIES_BY_COUNTER:
            raise RuntimeError(f"Duplicate counter: {self._counter}")
        Entity.ENTITIES_BY_COUNTER[self._counter] = self

        # Instantiate all contents.
        for e in self._data.contents:
            content_data = EntityData.evaluate_dynamic_entity(self._data, e)
            if not content_data:
                raise RuntimeError(f"Bad contents in {self.id} - {e}")
            instance = content_data.instantiate()
            instance.move(self)

    def move(self, new_environment) -> None:
        # TODO: Check if can be moved at all.
        # TODO: Check if can leave existing environment.
        # TODO: Check if can enter new environment.
        # If all passes, then:
        if self._environment:
            self._environment._contents.remove(self)
        self._environment = new_environment
        if self._environment:
            if self not in self._environment._contents:
                self._environment._contents.append(self)

    def __str__(self) -> str:
        env_id = "None"
        if self.environment:
            env_id = self.environment.id
        return f"<{self.__class__.__name__} id={self.id} env={env_id}>"

    @property
    def environment(self) -> Optional[Entity]:
        return self._environment

    @property
    def contents(self) -> Iterable[Entity]:
        return self._contents

    @property
    def counter(self) -> int:
        return self._counter

    @property
    def id(self) -> str:
        return self._data.id

    @property
    def name(self) -> str:
        return self._data.name

    @property
    def description(self) -> str:
        return normalize_paragraphs(self._data.description)

    @property
    def properties(self) -> EntityData.Properties:
        return self._data.properties


def find_data(id: str) -> Optional[EntityData]:
    if id not in EntityData.ENTITY_DATA:
        return None
    return EntityData.ENTITY_DATA[id]


def find_entities(id: str) -> Optional[list[Entity]]:
    if id not in Entity.ENTITIES:
        return None
    result = list(Entity.ENTITIES[id])
    if not result:
        return None
    return result


def find_entity(id: str) -> Optional[Entity]:
    entities = find_entities(id)
    if not entities:
        return None
    return entities[0]


def reset():
    EntityData.ENTITY_DATA.clear()
    Entity.ENTITIES.clear()
