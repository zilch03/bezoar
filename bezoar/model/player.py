from __future__ import annotations
from bezoar.model.location import Location

from typing import Optional

from bezoar.model import Emitter
from bezoar.model.entity import Entity, EntityData, find_data, find_entity


class PlayerData(EntityData):
    """
    The static data associated with the Player.
    """

    PLAYER_ID = "_*_PLAYER_*_"

    def __init__(
            self,
            name: EntityData.DynamicString,
            description: EntityData.DynamicString,
            contents: EntityData.Contents,
            properties: EntityData.Properties,
    ):
        if find_player_data() is not None:
            raise ValueError("Only one player declaration allowed.")
        super().__init__(
            id=PlayerData.PLAYER_ID,
            name=name,
            description=description,
            contents=contents,
            properties=properties)

    def instantiate(self) -> Player:
        return Player(self)


class Player(Entity):
    def __init__(self, data: PlayerData) -> None:
        if find_player() is not None:
            raise ValueError("Only one player instance allowed.")
        self._player_data = data
        super().__init__(data)

    def look(self, emit:Emitter) -> None:
        env = self.environment
        if not env:
            emit("You are nowhere! <This indicates a bug.>")
            return
        if isinstance(env, Location):
            env.emit_look(emit)
        else:
            emit(env.name)
            emit("")
            emit(env.description)



def player(
        name: EntityData.DynamicString,
        description: EntityData.DynamicString,
        contents: EntityData.Contents = [],
        properties: EntityData.Properties = {},
):
    return PlayerData(
        name=name,
        description=description,
        contents=contents,
        properties=properties)


def find_player_data() -> Optional[PlayerData]:
    maybe_player = find_data(PlayerData.PLAYER_ID)
    if maybe_player is None:
        return None
    if isinstance(maybe_player, PlayerData):
        return maybe_player
    return None


def find_player() -> Optional[Player]:
    maybe_player = find_entity(PlayerData.PLAYER_ID)
    if maybe_player is None:
        return None
    if isinstance(maybe_player, Player):
        return maybe_player
    return None
