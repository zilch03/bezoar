from __future__ import annotations

from typing import Callable, Optional, Union

from bezoar.model import Emitter
from bezoar.model.entity import Entity, EntityData, find_data, find_entity


class LocationData(EntityData):
    """
    A place that the player can be, along with other non-Location entities.
    Locations can never themselves have a Location.
    """

    DynamicLocation = Union['LocationData',
                            Callable[['LocationData'], 'LocationData']]
    Exits = dict[str, Union[str, DynamicLocation]]

    def __init__(
        self,
        id: str,
        name: EntityData.DynamicString,
        description: EntityData.DynamicString,
        exits: Exits,
        contents: EntityData.Contents,
        properties: EntityData.Properties
    ) -> None:
        super().__init__(
            id=id,
            name=name,
            description=description,
            contents=contents,
            properties=properties)
        self._raw_exits = exits

    def resolve_links(self) -> None:
        pass

    def instantiate(self) -> Location:
        return Location(self)

    def create(self) -> 'Location':
        return Location(self)


class Location(Entity):
    def __init__(self, data: LocationData) -> None:
        self._location_data = data
        super().__init__(data)

    def emit_look(self, emit: Emitter) -> None:
        emit(self.name)
        emit("")
        emit(self.description)


def location(
    id: str,
    name: EntityData.DynamicString,
    description: EntityData.DynamicString,
    exits: LocationData.Exits,
    contents: EntityData.Contents = [],
    properties: EntityData.Properties = {}
):
    return LocationData(id=id,
                        name=name,
                        description=description,
                        exits=exits,
                        contents=contents,
                        properties=properties)


def find_location_data(id: str) -> Optional[LocationData]:
    maybe_location = find_data(id)
    if maybe_location is None:
        return None
    if isinstance(maybe_location, LocationData):
        return maybe_location
    return None


def find_location(id: str) -> Optional[Location]:
    maybe_location = find_entity(id)
    if maybe_location is None:
        return None
    if isinstance(maybe_location, Location):
        return maybe_location
    return None
