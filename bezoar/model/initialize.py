from bezoar.model.entity import EntityData
from bezoar.model.location import LocationData
from bezoar.model.player import find_player, find_player_data


def initialize_model() -> None:
    for e in EntityData.ENTITY_DATA.values():
        e.resolve_links()

    # Instantiate Locations
    for e in EntityData.ENTITY_DATA.values():
        if isinstance(e, LocationData):
            e.instantiate()

    # Instantiate Player, if necessary
    player = find_player()
    if not player:
        player_data = find_player_data()
        if not player_data:
            raise RuntimeError("No player data defined.")
        player_data.instantiate()
