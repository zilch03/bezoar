"""
The Parser.

See [parser.md](parser.md).
"""

from typing import Optional, Tuple

from bezoar.model.entity import Entity
from bezoar.base.text import split_words

# These are the canonical direction forms that you should refer to when
# declaring exits. Standard synonyms will automatically be applied.
_CANONICAL_DIRECTIONS = {
    "aft",
    "down",
    "east",
    "fore",
    "in",
    "north",
    "northeast",
    "northwest",
    "out",
    "port",
    "south",
    "southeast",
    "southwest",
    "starboard",
    "up",
    "west",
}

# These are direction abbreviations or synonyms that will automatically be
# applied when typed by the user in a direction context.
_DIRECTION_SYNONYMS = {
    "d": "down",
    "e": "east",
    "enter": "in",
    "exit": "out",
    "n": "north",
    "ne": "northeast",
    "nw": "northwest",
    "s": "south",
    "se": "southeast",
    "sw": "southwest",
    "u": "up",
    "w": "west",
}

# These are verbs that can precede a direction to mean to take that exit from
# the location.
_MOVE_VERBS = {
    "go",
    "move",
    "travel",
    "walk",
}

# These are the prepositions that you should refer to when declaring action
# syntax. These were chosen by the criteria of being short, one word,
# and common in contemporary English usage.
_CANONICAL_PREPOSITIONS = {
    "about",
    "across",
    "at",
    "after",
    "around",
    "away from",
    "before",
    "behind",
    "between",
    "from",
    "in front of",
    "in",
    "out",
    "over",
    "near",
    "on",
    "opposite",
    "to",
    "towards",
    "under",
    "with",
}

# These are prepositions that are mostly universally synonyms for other
# prespositions.
_PREPOSITION_SYNONYMS = {
    "above": "on",
    "across from": "opposite",
    "beside": "near",
    "close to": "near",
    "inside": "in",
    "into": "in",
    "next to": "near",
    "on top of": "on",
    "onto": "on",
    "on to": "on",
    "outside of": "out",
    "regarding": "about",
}

# Words that indicate the beginning of a new sentence.
_SENTENCE_TERMINATORS = {
    "then",
    ".",
    ";",
}

# Articles that might be used with nouns to indicate specificity.
_ARTICLES = {
    "a",
    "an",
    "the",
}

# Conjunctions used to group
_CONJUNCTIONS = {
    "and",
    "because",
    "but",
    "however",
    "nor",
    "or",
    "yet",
}

# Nouns that the parser will have to figure out what is being referred to.
_PRONOUNS = {
    "it",
    "they",
    "he",
    "she"
}

Word = str
Sentence = list[Word]


class ParseResult(object):
    """
    The complete results of a parse, as applied to the context of the player.
    """

    def __init__(
        self,
        verb: str,
        preposition: Optional[str],
        direct_object: Optional[Entity],
        indirect_object: Optional[Entity],
        remainder: str,
    ) -> None:
        super().__init__()
        self.verb = verb
        self.preposition = preposition
        self.direct_object = direct_object
        self.indirect_object = indirect_object
        self.remainder = remainder


def parse(submission: str) -> Optional[ParseResult]:
    words = split_words(submission)
    if words[0] in _SENTENCE_TERMINATORS:
        words = words[1:]
    # TODO: Obviously incomplete.
    return None
