# The Parser

The parser attempts to parse imperative sentences, based heavily on the player's
current context.

In terms of sentence structure, all commands are in imperative form; meaning the
"tacit you" is always the subject.

The position of the parser is to act as an intermediary between the actual user
and the user's avatar in the game; as if one's brain had to communicate to one's
body as two entities. It ends up feeling more natural than it sounds.

## Definitions

Here are some technical definitions relevant to the domain. I had to look up the
precise meanings of a lot of these, so I wanted to make sure I had a good
reference.

- **noun** - A word or phrase referring to a person, place, thing, or idea.\
  _Examples_
  - "you" (pronoun person)
  - "Jerry" (person)
  - "home" (place)
  - "table" (thing)
  - "liberty" (idea)
- **pronoun** - A noun that indicates specificity based on context.\
  _Examples_
  - "you" (second person)
  - "myself" (first person reflexive)
  - "mine" (first person possessive)
  - "him" (third person male object)
  - "she" (third person female subject)
  - "it" (third person neuter)
- **reflexive pronoun** - A pronoun that refers to the sentence subject as the
  object.\
  _Examples: "yourself," "myself," and "itself."_
- **verb** - A word describing an action.\
  _Examples: "runs," "hopping," "take," and "fell."_
- **reflexive verb** - A verb whose subject and object refer to the same thing.\
  _Examples: "jump," "sit," "wave."_
- **imperative mood** - A language form that gives a direct order or command.\
  _Examples: "Give the ball to me." "Hit the tree with the axe." "Take the
  wood." "Jump over the stump." "Clap."_
- **imperative verb** - A verb conjugation that creates an imperative sentence.
  The imperative mood uses the zero infinitive form, which (with the exception
  of be) is the same as the second person in the present tense.\
  _Examples: "give," "hit," "take," "jump."_
- **preposition** - A preposition is a word or group of words used before a
  noun, pronoun, or noun phrase to show direction, time, place, location,
  spatial relationships, or to introduce an object.\
  _Examples: "in," "at," "on," "of," and "to."_
- **article** - A word used with a noun to limit or give definiteness.\
  _Examples: "a," "an," and "the."_

## Cases

We are thankfully only interested in the imperative mood, so this eliminates
large swaths of the English language. There are still a lot of cases to
consider, as well as some text adventure conventions that we need to preserve.

### Movement

In a text adventure, the world is split into discrete locations, and there are
discrete connections between locations, generally represented by a direction. As
these are the most commonly used commands, they have standard abbreviations, and
it is not required to use a standard imperative form.

> Go inside.

> Travel south.

This can be abbreviated to just the direction.

```none
> in
```

or

```none
> south
```

Respectively. For cardinal directions, just the first letter will do.

```none
> s
```

### Reflexive verbs with no preposition

The next simplest case is for reflexive verbs that don't have any preposition.

> Jump.

> Look.

This really means:

> (you) Jump (yourself).

> (you) Look (yourself).

We would never actually say this this way, but this is what is actually
happening from a sentence diagram perspective. The subject, "you," is also the
direct object, "yourself". Note that this is different from

> Look at yourself.

which actually means

> (you) Look (yourself) at yourself.

Presumably in a mirror... see a later case for prepositions.

Some verbs can be used reflexively or not.

> Shave.

means

> Shave (yourself).

which you can also say, and it wouldn't sound too weird. But, you can also

> Shave the orangutan.

Though, they wouldn't probably be very happy about it.

### Reflexive verbs with a preposition and no indirect object

Note that a preposition can change the meaning of a verb, so when parsing we
need to take prepositions into account.

> Jump forward.

> Jump up.

### Verbs with a direct object

This is the most common case of interacting with objects in the world.

> Get the lamp.

> Drink the water.

Adding object references now means we have to deal with articles, units,
multiple objects, adjectives, wildcards, and exceptions! Yikes. Let's break
these out into separate cases.

### Articles

Generally, we expect the user to be specific, and can complain if they aren't.

> Get a lamp.

Well, there's only one lamp here, so I guess you mean that one, but that's a
weird way to say it. Normally, we'd expect:

> Get the lamp.

But, because we are a game and not trying to be overly-prescriptive, we will
accept:

```none
> get lamp
```

### Adjectives

Each entity will may have one or more adjectives associated with it. This will
allow the parser to disambiguate ojects that have the same basic nomenclature.

Let's say from the example above there were two lamps, a floor lamp and a table
lamp. We'd have to ask for an adjective.

```none
> get lamp

Which lamp, the table lamp or the floor lamp?

> floor

The floor lamp is too awkward to carry.
```

Here we can just give the user another prompt, and they can ignore the question
or they can answer it with a naked adjective.

### Non-count nouns

Non-count nouns are nouns that are not counted, like water, salt, information.
In these cases, either no article or the definite article is used, or an amount.

> Drink water.

> Drink the water.

> Drink some water.

> Drink all the water.

### Multiple objects

Especially for verbs like "get," the player will want to be able to easily get
multiple things at once.

> Get the bottle, knife, and sack lunch.

### Reflexive verbs with a preposition and an indirect object

> Jump over the pit.

> Look at the leaflet.

> Look at yourself.

Prepositions can be multiple words, and this can also change the meaning
entirely of the verb, even if some of the words are the same.

> Stand in the circle.

> Stand in front of the circle.

### Verbs with a direct object and an indirect object

This is pretty much the most complex scenario.

> Throw the ball to Jerry.

> Look at myself in the mirror.

> Drink some water from the green bottle.

### Multiple sentences

The strategy for multiple sentences is to consume the input incrementally and if
we can't consume across a sentence separator, we will leave it and try again
after we execute the first sentence.

## EBNF Grammar

My strategy is to define a grammar for what I'm trying to accept, and then write
a formal parser to do it. Luckily, I have a lot of leeway since I know the size
of the token stream is very small and in memory, so I can look ahead all I want
and so on.

```ebnf
direction = "north" | "south" | ... ;
go-verb = "go" | "move" | ... ;
go-command = [ go-verb ] direction ;

imperative = go-command
           | reflexive-verb
           | verb direct-noun
           | reflexive-verb preposition indirect-noun
           | verb direct-noun preposition indirect-noun
           ;
```
