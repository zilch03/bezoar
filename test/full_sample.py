import logging

from bezoar import find_player_data, location, player, reset
from bezoar.console_runner import run

reset()

the_player = player(
    name="yourself",
    description="""
        You see yourself.
        """
)

location(
    id="house_west",
    name="West of House",
    description="""
        You are in front of a dilapidated house to the east, rotting
        away in the middle of a forest. The few flakes of paint that
        remain suggest that it was once painted white, but the house now
        bares its deteriorating structure to the elements.

        A once boarded-up door has largely disintegrated, leaving a gap
        you think you could squeeze through.
        """,
    exits={
        "north": "house_north",
        "south": "house_south",
        "west|in": "house_living_room",
    },
    contents=[
        the_player,
    ]
)


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    run()
