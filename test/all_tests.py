import test
import unittest

if __name__ == '__main__':
    runner = unittest.TextTestRunner(verbosity=2)
    result = runner.run(test.suite())
    if not result.wasSuccessful():
        exit(1)
