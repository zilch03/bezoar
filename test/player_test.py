from bezoar.model.player import find_player_data
import unittest

from bezoar import find_player, player, reset


class PlayerTest(unittest.TestCase):
    def setUp(self):
        reset()

    def tearDown(self):
        reset()

    def test_no_player(self):
        self.assertIsNone(find_player())

    def test_define_player(self):
        expected_name = "playername"
        expected_desc = "playerdesc"
        p = player(name=expected_name,
                   description=expected_desc)
        self.assertIsNotNone(p)
        self.assertEqual(p, find_player_data())
        self.assertEqual(expected_name, p.name)
        self.assertEqual(expected_desc, p.description)


if __name__ == '__main__':
    unittest.main()
