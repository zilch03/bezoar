from setuptools import setup

setup(name='bezoar',
      version='0',
      description='A library for declaratively defining text adventures.',
      url='https://gitlab.com/iffyb/bezoar',
      author='David Ghandehari',
      author_email='iffy@xarble.org',
      license='Apache 2.0',
      packages=['bezoar'],
      zip_safe=False)
