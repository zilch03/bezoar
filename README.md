# Bezoar

A Python 3 library for declaratively creating text adventure games.

> A bezoar (BE-zor) is a solid mass of indigestible material that accumulates in
your digestive tract, sometimes causing a blockage.\
> *- Mayo Clinic*

also

> **1540s**, "stone used as an antidote against poison," via Medieval Latin,
> from Arabic *bazahr*, from Persian *pad-zahr* "counter-poison," from *pad*
> "protecting, guardian, master" [...] + *zahr* "poison" [...]\
> *- [Online Etymology Dictionary](https://www.etymonline.com/word/bezoar)*

## Overview

Bezoar takes its influence from declarative build systems (specifically
[Bazel](http://bazel.build)), but instead of building a model of a project to
build, it builds a model of a text adventure.

## Strategy

1. Define Bezoar Definition Language (BDL) as a subset of Python
1. Implement Python backing for BDL
1. Port Infocom's 2nd-gen text parser (written in
   ZIL[<sup>1</sup>](#footnote-zil)) to Python <a name="notesource-zil"></a>
1. Create BDL parser/evaluator, perhaps based on Bazel BUILD file parser, get
   away from the Python runtime
1. ZIP bytecode generation
1. Support for advanced features, graphics, etc

## Bezoar Definition Language

The idea is to combine a declarative syntax with behavior, in much the same way
that MDL/ZIL did in the days of Infocom's Zork.

***Note:** This is currently in flux.*

### Fundamental Types

There are these fundamental entity types in BDL:

1. **Location** - A place that the player can be, along with other non-Location
                  entities.
   * AKA: Room
1. **Item** - An item that the player can take and have in their inventory.
   * AKA: Thing, Object
1. **Feature** - An interactive feature of a *Location* that can't be taken, but
                 is otherwise very similar to an *Item*.
1. **Actor** - A mobile object that can operate autonomously.
   * AKA: NPC, Monster, Mob
1. **Player** - An object representing player and game state that has no other
   locality.
1. **Action** - A command that the player can invoke, in some cases on a direct
                object.

### Mix-in Types

In addition, there are predefined and user-defined mix-in types that can be
added to the above fundamental types, as one way of reusing code. Since these
can be extended, or replaced, the list here is really just a subset of examples.

* **Container** - An object that can contain other objects.
* **Openable** - An object that can be opened and closed.

### Sample Syntax

```python
# The location() function declares a location, and returns a handle to that
# location, which can then be used to refer to that location directly, without
# doing a string lookup.
house_west = location(
  # A globally unique ID that can be used to look-up the location from
  # elsewhere in the code.
  id="house_west",

  # The canonical name of the location, though this need not be unique.
  name="West of House",

  # The long description of the location. Long descriptions are parsed as
  # Markdown.
  description="""
  You are in front of a dilapidated house to the east, rotting away in the
  middle of a forest. The few flakes of paint that remain suggest that it was
  once painted white, but the house now bares its deteriorating boards to the
  elements.

  A once boarded-up door has now fallen to the ground, leaving a gap you think
  you could squeeze through.
  """,

  # A dictionary of visible exits from the location.
  exits={
    # Common directions will automatically be aliased to their standard
    # shortcuts. 'n' for "north," etc...
    # String values are assumed to be IDs of location objects.
    "north": "house_north",

    # An explicit, but equivalent, form of linking by ID.
    "south": find("house_south"),

    # Pipes can be used to separate multiple directions that go to the same
    # location.
    "west|in": "house_living_room",
  },

  contents=[
    # You can declare features or items inline, like this `mailbox`, as well as
    # separately, such as the `leaflet` below.
    feature(
      # The globally-unique ID of the object that can be used to refer to the
      # object from anywhere else in the code.
      id="mailbox",

      # The primary, canonical name of the object. Used when listing in
      # locations and inventory.
      name="mailbox",

      # Adjectives that can be used to disambiguate the object.
      adjectives=[
        "small",
      ],

      # The long description of the object. Long descriptions are parsed as
      # Markdown.
      description="""
      You see a small, traditional-looking mailbox lying on the ground, next to
      a desiccated wooden post that was once its home.
      """,

      # Alternate nouns that can be used to refer to the object.
      aliases=[
        "box",
        "mailbox",
      ],

      # A dictionary of actions that can be performed when the object is present
      # in the same location as the player.
      present_actions={
        "open": mailbox_open,
        "close": mailbox_close,
      },

      # A list of all contents of the object.
      contents=[
        # String values are assumed to be the IDs of non-location objects. It is
        # not legal for a location to itself have a location.
        "leaflet",
      ],

      # Keyword Arguments (kwargs) are added as custom fields to the target
      # object.
      custom_1="foo",
    ),
  ],
)

# Objects may be declared in any order. Links will be resolved in a second pass,
# once all entities have been parsed.
item(
  id="leaflet",
  name="leaflet",
  adjectives=[
    "small",
    "paper",
  ],
  aliases=[
    "brochure",
    "letter",
    "memo",
  ],
  short_description="small leaflet",
  description="""
  # WELCOME TO BEZOAR

  Bezoar is a framework for building text adventure games, using the modern
  trappings of software technology that have emerged in the ~40 years since the
  original Zork.

  Bezoar is open-source, licensed under the Apache 2.0 Open Source License.

  Copyright (c) 2020 The Bezoar Contributors
  """,
)
```

### Links to other documents

* [Parser design - `bezoar/runtime/parser.md`](bezoar/runtime/parser.md)

----

<a name="footnote-zil">**Footnote 1**</a>: ZIL stands for "Zork Implementation
Language." Stu Galley rewrote the Infocom parser in ZIL, whereas the first
parser had to be ported to each platform along with the rest of the Z-Machine
interpreter. [↩](#notesource-zil)
