# Contributing to Bezoar

We'd love to hear about how you would like to contribute to Bezoar! It's worth
reading through this document first, to understand the process and to make sure
you know what to expect.

## Before You Contribute

By contributing code to Bezoar, you are agreeing to the [Contributor License
Agreement](CLA.md).

### What is a CLA?

A Contributor License Agreement is necessary mainly because you own the
copyright to your changes, even after your contribution becomes part of our
codebase, so we need your permission to use and distribute your code. We also
need to be sure of various other things — for instance that you'll tell us if
you know that your code infringes on other people's patents.

### Code Reviews

All submissions outside of maintainers requires code review. We
currently use GitLab's [Merge Request](https://gitlab.com/iffyb/bezoar/-/merge_requests) system.

## Submission Process

  1. Read the CLA and make sure you agree to it (see "Before You Contribute"
     above).
  1. Rebase your changes down into a single git commit.
  1. Create a Merge Request.
  1. Someone from the maintainers team will review the code, putting up comments
     on any things that need to change for submission.
  1. If you need to make changes, make them locally, test them, then `git commit
     --amend` to add them to the *existing* commit. Then return to step 2.
  1. Once the maintainer accepts the changes, they will rebase the MR into
     the target branch.
